#Base images
FROM python:3

#Source code
ADD src1/my_script_1.py /
ADD src1/my_script_2.py /
ADD src2/my_script_3.py /
ADD src2/my_script_4.py /

# No need to modify
ADD run.sh /			

#Dependencies
RUN pip install numpy

# No need to modify
RUN chmod +x /run.sh 

# No need to modify
CMD ["/run.sh"]
